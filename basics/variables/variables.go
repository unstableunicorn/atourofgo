package main

import "fmt"

var c, python, java bool // Module variable declaration

func main() {
	var i int // funct variable declaration
	fmt.Println(i, c, python, java)
}
