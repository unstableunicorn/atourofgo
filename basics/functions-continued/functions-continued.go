package main

import "fmt"

func mult(x, y int) int {
	return x * y
}

func main() {
	fmt.Println(mult(7, 6))
}
