package main

import (
	"fmt"
)

func Sqrt(x float64) float64 {
	z := 1.0
	i := 0
	for a := z; a >= 0.00001; i++ {
		z -= (z*z - x) / (2 * z)
		a = z*z - x
	}
	fmt.Printf("Count %v\n", i)
	return z
}

func main() {
	fmt.Println(Sqrt(2))
}
