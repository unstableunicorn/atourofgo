package main

import "fmt"

func main() {
	sum := 1
	// for ; sum < 1000; { // auto format removes this
	for sum < 1000 {
		sum += sum
	}
	fmt.Println(sum)
}
