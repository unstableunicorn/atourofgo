package main

import (
	"strings"

	"golang.org/x/tour/wc"
)

func WordCount(s string) map[string]int {
	fields := strings.Fields(s)
	scount := make(map[string]int)
	for _, v := range fields {
		scount[v] = strings.Count(s, v)

	}
	return scount
}

func main() {
	// fmt.Println(WordCount("I Am Groot Groot"))
	wc.Test(WordCount)
}
