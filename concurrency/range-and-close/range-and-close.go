package main

import (
	"fmt"
)

func fibonacci(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		c <- x
		x, y = y, x+y
	}
	close(c)
}

func main() {
	c := make(chan int, 10)
	go fibonacci(cap(c), c) //cap tells you the capacity of the buffer/array etc, len tell you how many items are in it
	for i := range c {
		fmt.Println(i)
	}
}
