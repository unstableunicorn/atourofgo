package main

import (
	"fmt"
	"reflect"
	"sort"

	"golang.org/x/tour/tree"
)

// type Tree struct {
// Left  *Tree
// Value int
// Right *Tree
// }

// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan int) {
	if t == nil {
		return
	}
	ch <- t.Value
	go Walk(t.Left, ch)
	go Walk(t.Right, ch)
}

// Same determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *tree.Tree) bool {
	ch1 := make(chan int)
	ch2 := make(chan int)
	go Walk(t1, ch1)
	go Walk(t2, ch2)
	ch1arr := make([]int, 10)
	ch2arr := make([]int, 10)
	for i := 0; i < 10; i++ {
		ch1arr[i], ch2arr[i] = <-ch1, <-ch2
	}
	sort.Ints(ch1arr)
	sort.Ints(ch2arr)
	fmt.Printf("Array 1: %v\n", ch1arr)
	fmt.Printf("Array 2: %v\n", ch2arr)
	return reflect.DeepEqual(ch1arr, ch2arr)
}

func main() {
	issame := Same(tree.New(1), tree.New(1))
	fmt.Printf("Trees are same?: %v\n", issame)
	issame = Same(tree.New(2), tree.New(3))
	fmt.Printf("Trees are same?: %v\n", issame)
}
