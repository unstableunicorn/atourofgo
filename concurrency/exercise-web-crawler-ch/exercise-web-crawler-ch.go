package main

import (
	"fmt"
	"sync"
)

type Fetcher interface {
	// Fetch returns the body of URL and
	// a slice of URLs found on that page.
	Fetch(url string) (body string, urls []string, err error)
}

// map to store urls that have been visited and a count of how many times.
type urlCount struct {
	v   map[string]int
	mux sync.Mutex
}

// Check if the url has been visited and increment count if so
func (c *urlCount) CheckVisited(key string) int {
	c.mux.Lock()
	defer c.mux.Unlock()
	// Lock so only one goroutine at a time can access the map c.v.
	c.v[key]++
	return c.v[key]
}

func (c *urlCount) notVisted(u []string) []string {
	var newu []string
	for _, v := range u {
		if c.CheckVisited(v) == 1 {
			newu = append(newu, v)
		}
	}
	return newu
}

// Crawl uses fetcher to recursively crawl
// pages starting with url, to a maximum of depth.
func Crawl(url string, depth int, fetcher Fetcher, uc *urlCount, ret chan string) {
	defer close(ret)
	if depth <= 0 {
		return
	}

	body, urls, err := fetcher.Fetch(url)
	if err != nil {
		ret <- err.Error()
		return
	}

	ret <- fmt.Sprintf("found: %s %q", url, body)
	notVisited := uc.notVisted(urls)
	result := make([]chan string, len(notVisited))
	for i, u := range notVisited {
		result[i] = make(chan string)
		go Crawl(u, depth-1, fetcher, uc, result[i])
	}

	for i := range result {
		for s := range result[i] {
			ret <- s
		}
	}

	return
}

func main() {
	result := make(chan string)
	c := urlCount{v: make(map[string]int)}
	go Crawl("https://golang.org/", 4, fetcher, &c, result)

	for s := range result {
		fmt.Println(s)
	}

}

// fakeFetcher is Fetcher that returns canned results.
type fakeFetcher map[string]*fakeResult

type fakeResult struct {
	body string
	urls []string
}

func (f fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := f[url]; ok {
		return res.body, res.urls, nil
	}
	return "", nil, fmt.Errorf("not found: %s", url)
}

// fetcher is a populated fakeFetcher.
var fetcher = fakeFetcher{
	"https://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"https://golang.org/pkg/",
			"https://golang.org/cmd/",
		},
	},
	"https://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"https://golang.org/",
			"https://golang.org/cmd/",
			"https://golang.org/pkg/fmt/",
			"https://golang.org/pkg/os/",
		},
	},
	"https://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
	"https://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"https://golang.org/",
			"https://golang.org/pkg/",
		},
	},
}
