package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// Return nil example
func (v Vertex) print() {
	fmt.Printf("X: %.3f, Y: %.3f", v.X, v.Y)
}

func main() {
	v := Vertex{3, 4}
	fmt.Println(v.Abs())
	v.print()
}
