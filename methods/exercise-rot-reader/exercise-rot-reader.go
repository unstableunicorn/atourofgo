package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	r io.Reader
}

// func (rotis *rot13Reader) Read(b []byte) (int, error) {
// 	// ASCII Table A->Z is 66->90 and a->z is 97->122
// 	n, err := rotis.r.Read(b)
// 	for i := 0; i < n; i++ {
// 		vofi := b[i]
// 		if (vofi >= 66 && vofi <= 90) || (vofi >= 97 && vofi <= 122) {
// 			vofi += 13
// 			if vofi > 90 && vofi < 97 {
// 				vofi += 7
// 			}
// 			if vofi > 122 {
// 				vofi = (vofi - 122) + 64
// 			}
// 		}
// 		b[i] = vofi
// 	}
// 	return n, err

// }

func (rotis *rot13Reader) Read(b []byte) (int, error) {
	// ASCII Table A->Z is 66->90 and a->z is 97->122
	n, err := rotis.r.Read(b)
	for i, v := range b {
		nv := v + 13
		switch {
		case (v < 65 || v > 122) || (v > 90 && v < 97):
			b[i] = v
		case nv > 90 && nv < 97:
			b[i] = v + 20
		case nv > 122:
			b[i] = v - 45
		default:
			b[i] = nv
		}
	}
	return n, err
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
