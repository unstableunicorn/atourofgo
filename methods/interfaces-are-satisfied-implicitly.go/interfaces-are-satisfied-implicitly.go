package main

import (
	"fmt"
)

type I interface {
	M()
	reverse()
}

type T struct {
	S string
}

// This method means type T implements the interface I,
// but we don't need to explicitly declare that it does so.
func (t T) M() {
	fmt.Println(t.S)
}

func (t T) reverse() {
	chars := []rune(t.S)
	for i, j := 0, len(chars)-1; i < j; i, j = i+1, j-1 {
		chars[i], chars[j] = chars[j], chars[i]
	}
	fmt.Println(string(chars))
}

func main() {
	var i I = T{"hello"}
	i.M()
	i.reverse()
}
