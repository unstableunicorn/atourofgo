package main

import "fmt"

type I interface {
	M()
}

func main() {
	var i I
	describe(i)
	// this will fail here with a panic as it has no idea which interface type M to call as there is no value
	i.M()
}

func describe(i I) {
	fmt.Printf("(%v, %T)\n", i, i)
}
