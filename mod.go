package main

// Putting this here so vs code doesn't complain about go modules not on path
// It expects the root project folder to have a go file.
import "fmt"

func main() {
	fmt.Println("Hello There! This is my go learning project")
}
